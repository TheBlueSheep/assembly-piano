
IDEAL
MODEL small
STACK 100h
p186
DATASEG
isDown db 0
;note dw 11EDh, 0FE8h, 0E2Bh, 0D5Bh, 0BE4h, 0A98h, 96Fh, 8E5h
note dw 8E5h, 7F4h, 715h, 6ADh, 5F2h, 54Ch, 4B7h, 472h
; --------------------------
; Your variables here
; --------------------------
CODESEG



proc PaintPixel
	push bp
	mov bp, sp
	pusha
	mov bh,0h
	mov cx, [bp +6] ;Xpaint
	mov dx, [bp+4] ;Ypaint
	mov ax, [bp+8] ;Colpaint is in low
	mov ah, 0Ch
	int 10h
	popa
	pop bp
	ret 6
endp PaintPixel


; t1 y, t2: x, t3: color, t4: len
proc PaintRow
	push bp
	mov bp, sp
	pusha
	mov cx, [bp+10]
	loopGo:
	push [bp+8];Colpa
	push [bp +6];Xpa
	push [bp+4];Ypa ; are removed at ret PaintPixel
	call PaintPixel
	mov ax,[bp+6]
	inc ax
	mov [bp+6],ax
	loop loopGo
	popa
	pop bp
	ret 8
endp PaintRow

; t1 y, t2: x, t3: color, t4: len, t5: width
proc PaintRectangle
	push bp
	mov bp, sp
	pusha
	mov cx, [bp+12]
	loopRec:
	push [bp +10]; Lenrec
	push [bp+8] ;Colrec
	push [bp +6] ; Xrec
	push [bp+4] ;Yrec ; are removed at ret PaintPixel
	call PaintRow
	mov ax,[bp+4]
	inc ax
	mov [bp+4], ax
	loop loopRec
	popa
	pop bp
	ret 10
endp PaintRectangle




;draws a key by key index
;push key index and then color
proc MakeKey
	push bp
	mov bp, sp
	pusha
	
	mov cx, [bp+4]
	mov ax, [bp+6];index
	
	push 100
	push 38
	
	push cx
	
	mov bx,40
	mul bl
	
	push ax
	push 100
	
	
	call PaintRectangle
	
	popa
	pop bp
	ret 4
endp MakeKey
;Draws The Board
proc PrepareBoard
	push bp
	mov bp, sp
	pusha
	mov cx,8
	mov ax,0

keys:
	push ax
	push 0fh
	call makeKey
	inc ax
	loop keys
	
	popa
	pop bp
	ret 0 
endp PrepareBoard

;plays a key
proc playKey
	push bp
	mov bp, sp
	pusha
	
	in al, 61h
	or al, 11b
	out 61h, al
	
	mov al, 0B6h
	out 43h, al
	
	mov bx, [bp+4]
	shl bx,1
	mov si, offset note
	mov ax, [si+bx]
	out 42h, al
	mov al, ah
	out 42h, al	
	
	
	popa
	pop bp
	ret 2
endp playKey


start:
	mov ax, @data
	mov ds, ax
; --------------------------
; Your code here
; --------------------------

	mov ax, @data
	mov ds, ax
	mov ax, 13h
	int 10h ; change to graphic mode
	
	call PrepareBoard
		
	
mainLoop:	
	
	in al,64h
	cmp al, 10b
	je mainLoop
	in al, 60h
	
	
	cmp al, 1
	je exit
	; check which key and scan code
	
	
	mov [isDown],1
	
	cmp al, 9Eh
	jl checkValid
	cmp al, 0A5h
	jg checkValid
	
upKey:
	mov [isDown],0
	
	
	
	

checkValid:
	shl al,1
	shr al,1
	sub ax, 1Eh
	cmp al, 0h
	jl mainLoop
	cmp al, 7h
	jg mainLoop
	
	
	; draw key
	xor ah,ah
	cmp[isDown], 0
	je clearKeyColor
	jne changeKey
	
clearKeyColor:
	push ax
	push 0fh
	call makeKey
;	close sound

	in al, 61h
	and al, 11111100b
	out 61h, al
	
	jmp mainLoop 
	

	; play sound
changeKey:
	push ax
	push 03h
	call makeKey
	
playSound:
	cmp [isDown], 1 
	jne mainLoop
	
;playing sound
	
	xor ah,ah
	push ax
	call playKey
	
	
	jmp mainLoop
	
	
	
exit:
	in al, 61h
	and al, 11111100b
	out 61h, al


	mov ax,2
	int 10h
	
	mov ax, 4c00h
	int 21h
END start


